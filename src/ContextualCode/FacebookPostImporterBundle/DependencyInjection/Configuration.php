<?php

namespace ContextualCode\FacebookPostImporterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cc_facebook_post_importer');

        $rootNode
            ->children()
                ->arrayNode("params")
                    ->children()
                    ->scalarNode("access_token")
                    ->end()
                    ->scalarNode("app_id")
                    ->end()
                    ->scalarNode("app_secret")
                    ->end()
                    ->scalarNode("page_id")
                    ->end()
                    ->scalarNode("facebook_version")
                        ->defaultValue("v2.12")
                    ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}

