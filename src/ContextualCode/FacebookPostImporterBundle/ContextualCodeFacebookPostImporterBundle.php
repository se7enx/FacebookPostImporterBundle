<?php

namespace ContextualCode\FacebookPostImporterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeFacebookPostImporterBundle extends Bundle
{
    protected $name = 'ContextualCodeFacebookPostImporterBundle';
}

