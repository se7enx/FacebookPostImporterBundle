<?php

namespace Project\SiteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use eZ\Publish\API\Repository\Values\Content\Content;

class ImportFBPostsCommand extends ContainerAwareCommand
{
    
    /**
     * Configures the command
     */
    protected function configure()
    {
        $this->setName('site:import_fb_posts');
        $this->setDefinition(
            array(
                new InputOption('containernodeid', null, InputOption::VALUE_REQUIRED, 'Node ID of the container for the imported posts.'),
                new InputOption('since', null, InputOption::VALUE_REQUIRED, 'Timestamp for the "since" value to the FB importer.'),
                new InputOption('until', null, InputOption::VALUE_REQUIRED, 'Timestamp for the "until" value to the FB importer.')
            )

        );
    }

    /**
     * Executes the command
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // set user as admin
        $repository = $this->getContainer()->get('ezpublish.api.repository');
        $userService = $repository->getUserService();
        $adminUser = $userService->loadUserByLogin('admin');
        $repository->setCurrentUser($adminUser);

        $output->writeln('<info>FB Post import starting...</info>');

        $since = $input->getOption('since');
        $until = $input->getOption('until');
        $containerNodeID = $input->getOption('containernodeid');
        if (!isset($containerNodeID)) {
            $output->writeln('<error>Please supply a `--containernodeid` option.</error>');
            return 1;
        }

        $output->writeln("<info>Importing to container node ID {$containerNodeID}...</info>");
        $output->writeln("<info>Since: {$since}...</info>");
        $output->writeln("<info>Until: {$until}...</info>");

        $fbImporter = $this->getContainer()->get('contextualcode.facebookpostimporter.importer');

        $fbImporter->setContainerNodeID($containerNodeID);
        $fbImporter->setImportClassID('fb_post');
        $fbImporter->setFBPostIDAttribute('fb_post_id');
        $fbImporter->setCallableForEdge(array('Project\SiteBundle\Command\ImportFBPostsCommand', 
                                              'fbImporterPostEdgeMapper') 
                                       );
        $fbImporter->setCallableForEdge(array('Project\SiteBundle\Command\ImportFBPostsCommand', 
                                              'fbImporterAttachmentEdgeMapper'),
                                        '/attachments'
                                       );

        $fbImporter->import($since, $until);

        $output->writeln('Done!');

    }


    public static function fbImporterPostEdgeMapper($data, &$contentCreateStruct)
    {
        if (!isset($data['message'])) {
            return false;
        }

        $contentCreateStruct->setField('fb_post_id', $data['id']);
        $contentCreateStruct->setField('message', self::wrapInXMLParagraph($data['message']));

        return true;
    }

    public static function fbImporterAttachmentEdgeMapper($data, &$contentCreateStruct)
    {
        if (count($data) == 0) {
            return true;
        }
        if (!isset( $data[0]['media']['image']['src'])) {
            return true;
        }

        $imgSrc = $data[0]['media']['image']['src'];
        $imgSrcHash = md5($imgSrc);
        $imgExt = pathinfo($imgSrc, PATHINFO_EXTENSION);
        $imgExt = preg_replace('/\?.*/', '', $imgExt);
        $filePath = "/tmp/{$imgSrcHash}.{$imgExt}";

        $success = self::downloadFile($imgSrc, $filePath);

        if (!$success) {
            return false;
        }
        
        $contentCreateStruct->setField('image', $filePath);

        return true;
    }


    private static function downloadFile($url, $path)
    {
        $newfname = $path;
        $file = fopen($url, 'rb');
        if ($file) {
            $newf = fopen($newfname, 'wb');
            if ($newf) {
                while (!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                }
            }

            fclose($file);

            if ($newf) {
                fclose($newf);
            }

            return true;
        }

        return false;
    }

    private static function wrapInXMLParagraph($str)
    {
        $str = htmlspecialchars($str, ENT_XML1, 'UTF-8');

        // this is for ezplatform...
        // ez5 would look like:
        // <?xml version='1.0' encoding='utf-8'?>
        // <section><paragraph>$str</paragraph></section>

        $xml = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<section xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml"
         xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom"
         version="5.0-variant ezpublish-1.0">
    <para>$str</para>
</section>
EOT;

        return $xml;
    }

}
